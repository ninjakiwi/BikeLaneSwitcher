﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public Text txtCenter;
	public bool pause;
	private Vector3 mvPos;

	public int countDown;
	public int spdMultiplier;
	private System.DateTime lastPedal, lastUpToSpeed;
	private System.TimeSpan travelTimePrev, timeSinceAtSpeed, timeSincePedal;
	private double spd;
	public double minSpd;
	private Vector3 spdVec;
	private Vector3 resetPos;
	public GameObject[] ground;

	// Use this for initialization
	void Start () {
		lastUpToSpeed = System.DateTime.UtcNow;
		lastPedal = System.DateTime.UtcNow.AddDays(-1);
		spd = 0;
		txtCenter.text = "";
		pause = false;
		mvPos = new Vector3(0, 0, 3);
		spdVec = new Vector3(1, 0, 0);
		resetPos = new Vector3(-600, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (pause) {
			if (Input.GetKeyDown(KeyCode.Space) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)) {
				lastUpToSpeed = System.DateTime.UtcNow;
				lastPedal = System.DateTime.UtcNow.AddDays(-1);
				spd = 0;
				pause = false;
			}
			return;
		}

		// Player side movements (touch)
		if (Input.touchCount > 0) {
            		Touch touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Ended) {
				// do stuff
				if (touch.position.x < Screen.width/2)
					movePlayer(true);
				else // on right
					movePlayer(false);
			}
		}
		
		// Player side movements (arrow keys)
		if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            		// do move left
			movePlayer(true);
		} else if (Input.GetKeyDown(KeyCode.RightArrow)) {
			// do move right
			movePlayer(false);
		} else if (Input.GetKeyDown(KeyCode.UpArrow)) {
			// do move right
			pedalTrigger();
		} else if (Input.GetKeyDown(KeyCode.Space)) {
			pause = true;
		}

		// Move ground
		for (int i = 0; i < 3; i ++) {
			ground[i].transform.position += spdVec * (int)spd * Time.deltaTime;
			if (ground[i].transform.position.x > 200) {
				ground[i].transform.position += resetPos;
			}
		}

		// Calc speed
		timeSincePedal = System.DateTime.UtcNow - lastPedal;
		if (travelTimePrev.TotalSeconds < timeSincePedal.TotalSeconds) {
			spd *= 0.99;
		} else {
			spd += 0.2 * (spdMultiplier / travelTimePrev.TotalSeconds - spd);
		}

		// Check speed is fast enough
		if (spd < minSpd) {
			timeSinceAtSpeed = System.DateTime.UtcNow - lastUpToSpeed;
			txtCenter.text = "Speed Up! " + (countDown - (int)timeSinceAtSpeed.TotalSeconds);
			if (timeSinceAtSpeed.TotalSeconds >= countDown) {
				looseGame();
			}
		} else {
			txtCenter.text = "";
			lastUpToSpeed = System.DateTime.UtcNow;
		}
	}

	public void pedalTrigger() {
		System.DateTime now = System.DateTime.UtcNow;
		travelTimePrev = now - lastPedal;  
		lastPedal = now;

		//spd = 10/travelTime.TotalSeconds;

		Debug.Log("travelTime: " + travelTimePrev );
	}

	void movePlayer(bool left) {
		if (left) {
			if (transform.position.z > -3)
				transform.position -= mvPos;
		} else {
			if (transform.position.z < 3)
				transform.position += mvPos;
		}
	}

	void OnTriggerEnter(Collider other) {
		looseGame();
    	}

	void looseGame() {
		// Pause game
		pause = true;
        	txtCenter.text = "Nice try :(";
	}
}
